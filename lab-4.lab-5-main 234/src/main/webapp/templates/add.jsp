<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add</title>
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Styles -->
    <style>
        body {
            padding-top: 50px;
            font-family: Arial, sans-serif;
            text-align: center;
        }
        h1 {
            margin-bottom: 30px;
            font-weight: bold;
            color: #007bff;
        }
        form {
            max-width: 500px;
            margin: 0 auto;
            background-color: #fff;
            padding: 30px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        label {
            display: block;
            margin-bottom: 10px;
            color: #333;
            font-weight: bold;
            text-align: left;
        }
        input[type="text"],
        input[type="date"],
        input[type="number"],
        select {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        input[type="submit"] {
            width: 100%;
            padding: 12px;
            border: none;
            border-radius: 4px;
            background-color: #007bff;
            color: white;
            cursor: pointer;
            transition: background-color 0.3s;
        }
        input[type="submit"]:hover {
            background-color: #0056b3;
        }
        .error {
            color: red;
            font-size: 12px;
            margin-top: 5px;
            text-align: left;
        }
    </style>
    <!-- JavaScript Validation -->
    <script>
        function validateForm() {
            var name = document.getElementById("name").value;
            var secondName = document.getElementById("secondName").value;
            var submitDate = document.getElementById("submitDate").value;
            var purchasePrice = document.getElementById("purchasePrice").value;
            var retailPrice = document.getElementById("retailPrice").value;

            if (name.match(/\d+/)) {
                document.getElementById("nameError").innerText = "Name should not contain numbers.";
                return false;
            } else {
                document.getElementById("nameError").innerText = "";
            }

            if (secondName.match(/\d+/)) {
                document.getElementById("secondNameError").innerText = "Second Name should not contain numbers.";
                return false;
            } else {
                document.getElementById("secondNameError").innerText = "";
            }

            var selectedDate = new Date(submitDate);
            var currentYear = new Date().getFullYear();
            if (selectedDate.getFullYear() !== currentYear) {
                document.getElementById("submitDateError").innerText = "Submit Date should be in the current year.";
                return false;
            } else {
                document.getElementById("submitDateError").innerText = "";
            }

            if (purchasePrice <= 0) {
                document.getElementById("purchasePriceError").innerText = "Purchase Price should be greater than 0.";
                return false;
            } else {
                document.getElementById("purchasePriceError").innerText = "";
            }

            if (retailPrice <= 0) {
                document.getElementById("retailPriceError").innerText = "Retail Price should be greater than 0.";
                return false;
            } else {
                document.getElementById("retailPriceError").innerText = "";
            }

            return true;
        }
    </script>
</head>
<body>
<div class="container">
    <h1>Add</h1>
    <form action="add" method="post" onsubmit="return validateForm()">
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" id="name" name="name" class="form-control" required>
            <span id="nameError" class="error"></span>
        </div>

        <div class="form-group">
            <label for="secondName">Second Name:</label>
            <input type="text" id="secondName" name="secondName" class="form-control" required>
            <span id="secondNameError" class="error"></span>
        </div>

        <div class="form-group">
            <label for="submitDate">Submit Date:</label>
            <input type="date" id="submitDate" name="submitDate" class="form-control" required>
            <span id="submitDateError" class="error"></span>
        </div>

        <div class="form-group">
            <label for="email">Email:</label>
            <input type="text" id="email" name="email" class="form-control" required>
            <span id="emailError" class="error"></span>
        </div>

        <div class="form-group">
            <label for="manufacturer">Manufacturer:</label>
            <input type="text" id="manufacturer" name="manufacturer" class="form-control" required>
            <span id="manufacturerError" class="error"></span>
        </div>

        <div class="form-group">
            <label for="model">Model:</label>
            <input type="text" id="model" name="model" class="form-control" required>
            <span id="modelError" class="error"></span>
        </div>

        <div class="form-group">
            <label for="platform">Platform:</label>
            <input type="text" id="platform" name="platform" class="form-control" required>
            <span id="platformError" class="error"></span>
        </div>

        <div class="form-group">
            <label for="camera">Camera:</label>
            <input type="text" id="camera" name="camera" class="form-control" required>
            <span id="cameraError" class="error"></span>
        </div>

        <div class="form-group">
            <label for="internet">Internet:</label>
            <select id="internet" name="internet" class="form-control" required>
                <option value="yes">Yes</option>
                <option value="no">No</option>
            </select>
            <span id="internetError" class="error"></span>
        </div>

        <div class="form-group">
            <label for="gpsModule">GPS Module:</label>
            <select id="gpsModule" name="gpsModule" class="form-control" required>
                <option value="yes">Yes</option>
                <option value="no">No</option>
            </select>
            <span id="gpsModuleError" class="error"></span>
        </div>

        <div class="form-group">
            <label for="dictaphone">Dictaphone:</label>
            <input type="text" id="dictaphone" name="dictaphone" class="form-control" required>
            <span id="dictaphoneError" class="error"></span>
        </div>
            <div class="form-group">
                <label for="purchasePrice">Purchase Price:</label>
                <input type="number" id="purchasePrice" name="purchasePrice" step="0.01" class="form-control" required>
                <span id="purchasePriceError" class="error"></span>
            </div>

            <div class="form-group">
                <label for="retailPrice">Retail Price:</label>
                <input type="number" id="retailPrice" name="retailPrice" step="0.01" class="form-control" required>
                <span id="retailPriceError" class="error"></span>
            </div>

            <input type="submit" value="Add" class="btn btn-primary">
    </form>
</div>
</body>
</html>
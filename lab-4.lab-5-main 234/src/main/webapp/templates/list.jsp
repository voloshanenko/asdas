<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Management</title>
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Styles -->
    <style>
        body {
            padding-top: 50px;
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            background-color: #f8f9fa;
            color: #333;
        }
        h1 {
            margin-bottom: 30px;
            font-weight: bold;
            color: #007bff;
        }
        .btn {
            margin: 5px;
            transition: all 0.3s ease-in-out;
        }
        .btn:hover {
            transform: scale(1.05);
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 30px;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 12px;
            text-align: left;
        }
        th {
            background-color: #007bff;
            color: #fff;
        }
        tr:hover {
            background-color: rgba(0, 123, 255, 0.1);
        }
        .table-responsive {
            overflow-x: auto;
        }
        .container {
            max-width: 1200px;
            margin: 0 auto;
        }
        .no-data {
            font-style: italic;
            color: #777;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Product Management</h1>
    <div class="row">
        <div class="col-md-6">
            <a href="add" class="btn btn-primary">Add New Product</a>
        </div>
    </div>
    <c:choose>
        <c:when test="${empty students}">
            <p class="no-data">No products found.</p>
        </c:when>
        <c:otherwise>
            <div class="table-responsive">
                <table class="table">
                    <caption>List of Products</caption>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Second Name</th>
                        <th>Submit Date</th>
                        <th>Email</th>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Platform</th>
                        <th>Camera</th>
                        <th>Internet</th>
                        <th>GPS Module</th>
                        <th>Dictaphone</th>
                        <th>Purchase Price</th>
                        <th>Retail Price</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="student" items="${students}">
                        <tr>
                            <td><c:out value="${student.name}" /></td>
                            <td><c:out value="${student.secondName}" /></td>
                            <td><c:out value="${student.submitDate}" /></td>
                            <td><c:out value="${student.email}" /></td>
                            <td><c:out value="${student.manufacturer}" /></td>
                            <td><c:out value="${student.model}" /></td>
                            <td><c:out value="${student.platform}" /></td>
                            <td><c:out value="${student.camera}" /></td>
                            <td><c:out value="${student.internet}" /></td>
                            <td><c:out value="${student.gpsModule}" /></td>
                            <td><c:out value="${student.dictaphone}" /></td>
                            <td><c:out value="${student.purchasePrice}" /></td>
                            <td><c:out value="${student.retailPrice}" /></td>
                            <td>
                                <a href="edit?id=${student.id}" class="btn btn-secondary">Edit</a>
                                <a href="delete?id=${student.id}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this product?');">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>

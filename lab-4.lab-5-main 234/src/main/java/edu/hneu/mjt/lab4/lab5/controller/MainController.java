package edu.hneu.mjt.lab4.lab5.controller;

import edu.hneu.mjt.lab4.lab5.dao.ProductDAO;
import edu.hneu.mjt.lab4.lab5.model.Product;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@WebServlet("/")
public class MainController extends HttpServlet {

    private ProductDAO studentDAO;

    public void init() {
        studentDAO = new ProductDAO();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        switch (action) {
            case "/":
                showHomePage(request, response);
                break;
            case "/add":
                showAddForm(request, response);
                break;
            case "/list":
                listStudents(request, response);
                break;
            case "/delete":
                deleteStudent(request, response);
                break;
            case "/edit":
                editStudent(request, response);
                break;
            default:
                showErrorPage(request, response);
                break;
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        switch (action) {
            case "/add":
                addStudent(request, response);
                break;
            case "/update":
                updateStudent(request, response);
                break;
        }
    }

    private void showHomePage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/templates/home.jsp").forward(request, response);
    }

    private void showAddForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/templates/add.jsp").forward(request, response);
    }

    private void addStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String secondName = request.getParameter("secondName");
        String surname = request.getParameter("surname");
        String submitDateStr = request.getParameter("submitDate");
        String email = request.getParameter("email");
        String manufacturer = request.getParameter("manufacturer");
        String model = request.getParameter("model");
        String platform = request.getParameter("platform");
        String camera = request.getParameter("camera");
        String internet = request.getParameter("internet");
        String gpsModule = request.getParameter("gpsModule");
        String dictaphone = request.getParameter("dictaphone");

        int mark = parseInteger(request.getParameter("mark"), 0);
        double purchasePrice = parseDouble(request.getParameter("purchasePrice"), 0.0);
        double retailPrice = parseDouble(request.getParameter("retailPrice"), 0.0);

        Date submitDate = parseDate(submitDateStr);

        Product student = new Product(name, secondName, surname, submitDate, mark, email, manufacturer, model, platform, camera, internet, gpsModule, dictaphone, purchasePrice, retailPrice);
        studentDAO.saveStudent(student);

        response.sendRedirect(request.getContextPath() + "/list");
    }

    private void listStudents(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Product> students = studentDAO.getAllStudents();
        request.setAttribute("students", students);
        request.getRequestDispatcher("/templates/list.jsp").forward(request, response);
    }

    private void deleteStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = parseInteger(request.getParameter("id"), -1);
        if (id != -1) {
            studentDAO.deleteStudent(id);
        }
        response.sendRedirect(request.getContextPath() + "/list");
    }

    private void editStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = parseInteger(request.getParameter("id"), -1);
        if (id != -1) {
            Product student = studentDAO.getStudent(id);
            request.setAttribute("student", student);
            request.getRequestDispatcher("/templates/edit.jsp").forward(request, response);
        } else {
            showErrorPage(request, response);
        }
    }

    private void updateStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = parseInteger(request.getParameter("id"), -1);
        String name = request.getParameter("name");
        String secondName = request.getParameter("secondName");
        String surname = request.getParameter("surname");
        String submitDateStr = request.getParameter("submitDate");
        String email = request.getParameter("email");
        String manufacturer = request.getParameter("manufacturer");
        String model = request.getParameter("model");
        String platform = request.getParameter("platform");
        String camera = request.getParameter("camera");
        String internet = request.getParameter("internet");
        String gpsModule = request.getParameter("gpsModule");
        String dictaphone = request.getParameter("dictaphone");

        int mark = parseInteger(request.getParameter("mark"), 0);
        double purchasePrice = parseDouble(request.getParameter("purchasePrice"), 0.0);
        double retailPrice = parseDouble(request.getParameter("retailPrice"), 0.0);

        Date submitDate = parseDate(submitDateStr);

        Product student = new Product(id, name, secondName, surname, submitDate, mark, email, manufacturer, model, platform, camera, internet, gpsModule, dictaphone, purchasePrice, retailPrice);
        studentDAO.updateStudent(student);

        response.sendRedirect(request.getContextPath() + "/list");
    }

    private void showErrorPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        RequestDispatcher dispatcher = request.getRequestDispatcher("/templates/error.jsp");
        dispatcher.forward(request, response);
    }

    private int parseInteger(String str, int defaultValue) {
        try {
            return str != null ? Integer.parseInt(str) : defaultValue;
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    private double parseDouble(String str, double defaultValue) {
        try {
            return str != null ? Double.parseDouble(str) : defaultValue;
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    private Date parseDate(String str) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return str != null ? sdf.parse(str) : null;
        } catch (ParseException e) {
            return null;
        }
    }
}

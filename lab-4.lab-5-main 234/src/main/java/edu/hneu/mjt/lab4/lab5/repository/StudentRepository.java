package edu.hneu.mjt.lab4.lab5.repository;

import edu.hneu.mjt.lab4.lab5.model.Product;

import java.util.List;

public interface StudentRepository {

    void saveStudent(Product student);

    void updateStudent(Product student);

    void deleteStudent(int id);

    Product getStudent(int id);

    List<Product> getAllStudents();
}

package edu.hneu.mjt.lab4.lab5.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "students")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "secondName")
    private String secondName;

    @Column(name = "surname")
    private String surname;

    @Column(name = "submitDate")
    @Temporal(TemporalType.DATE)
    private Date submitDate;

    @Column(name = "mark")
    private int mark;

    @Column(name = "email")
    private String email;

    @Column(name = "manufacturer")
    private String manufacturer;

    @Column(name = "model")
    private String model;

    @Column(name = "platform")
    private String platform;

    @Column(name = "camera")
    private String camera;

    @Column(name = "internet")
    private String internet;

    @Column(name = "gpsModule")
    private String gpsModule;

    @Column(name = "dictaphone")
    private String dictaphone;

    @Column(name = "purchasePrice")
    private double purchasePrice;

    @Column(name = "retailPrice")
    private double retailPrice;

    public Product() {}

    public Product(int id, String name, String secondName, String surname, Date submitDate, int mark, String email, String manufacturer, String model, String platform, String camera, String internet, String gpsModule, String dictaphone, double purchasePrice, double retailPrice) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.surname = surname;
        this.submitDate = submitDate;
        this.mark = mark;
        this.email = email;
        this.manufacturer = manufacturer;
        this.model = model;
        this.platform = platform;
        this.camera = camera;
        this.internet = internet;
        this.gpsModule = gpsModule;
        this.dictaphone = dictaphone;
        this.purchasePrice = purchasePrice;
        this.retailPrice = retailPrice;
    }

    public Product(String name, String secondName, String surname, Date submitDate, int mark, String email, String manufacturer, String model, String platform, String camera, String internet, String gpsModule, String dictaphone, double purchasePrice, double retailPrice) {
        this.name = name;
        this.secondName = secondName;
        this.surname = surname;
        this.submitDate = submitDate;
        this.mark = mark;
        this.email = email;
        this.manufacturer = manufacturer;
        this.model = model;
        this.platform = platform;
        this.camera = camera;
        this.internet = internet;
        this.gpsModule = gpsModule;
        this.dictaphone = dictaphone;
        this.purchasePrice = purchasePrice;
        this.retailPrice = retailPrice;
    }

    // Getters and setters for all fields

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getCamera() {
        return camera;
    }

    public void setCamera(String camera) {
        this.camera = camera;
    }

    public String getInternet() {
        return internet;
    }

    public void setInternet(String internet) {
        this.internet = internet;
    }

    public String getGpsModule() {
        return gpsModule;
    }

    public void setGpsModule(String gpsModule) {
        this.gpsModule = gpsModule;
    }

    public String getDictaphone() {
        return dictaphone;
    }

    public void setDictaphone(String dictaphone) {
        this.dictaphone = dictaphone;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public double getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(double retailPrice) {
        this.retailPrice = retailPrice;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", secondName='" + secondName + '\'' +
                ", surname='" + surname + '\'' +
                ", submitDate=" + submitDate +
                ", mark=" + mark +
                ", email='" + email + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", platform='" + platform + '\'' +
                ", camera='" + camera + '\'' +
                ", internet='" + internet + '\'' +
                ", gpsModule='" + gpsModule + '\'' +
                ", dictaphone='" + dictaphone + '\'' +
                ", purchasePrice=" + purchasePrice +
                ", retailPrice=" + retailPrice +
                '}';
    }
}

# lab 4-5 Servlets
Розробка веб додатку<br>Волошаненко Даніл<br>122.010.21.2<br>19.05.2024

## Зміст 

- [Технології](#технології)
- [Початок роботи](#Початок-роботи)
- [Структура](#Структура)
- [Використані Джерела](Використані-Джерела)


## Технології

- [Apache TomCat](https://tomcat.apache.org/)
- [Сервлет (Java)](https://habr.com/ru/articles/334138/)
- [Postgresql](https://www.postgresql.org/)
- [Maven](https://maven.apache.org/)

## Початок роботи

Завантаження та встановленння додаткових пакетів.

- [Video JavaGuides( How to install Maven)](https://www.youtube.com/watch?v=YTvlb6eny_0&ab_channel=JavaGuides)
- [Tomcat Install JavaGuides](https://www.youtube.com/watch?v=Tf-xyLBW9nI&ab_channel=PersonalClasses)
- [PostgreSQL 12 Install](https://www.youtube.com/watch?v=oEi5IUgxaU0)

Обов'язково через термінал вказати необхідну інформацію щодо БД
```sh
Server [localhst]:
Database [postgres]:
Port [5432]:
Username [postgres]:
```
Після налаштування серверу необхідно створити БД у pgAdmin4

![Створення БД](img/photo_2024-05-19_16-07-39.jpg)
Створення БД

[Start Project](https://www.youtube.com/watch?v=Tf-xyLBW9nI&ab_channel=PersonalClasses)

Відео-інструкція по початку роботи та створення першого проекту.

## Структура

Реалізація містить трьохріаневу архітектуру
- Шаблон DAO
- Шаблон MCV
- Рівень бізнес логіки

![Структура](img/photo_2024-05-19_16-13-52.jpg)

Структура проекту

## Використані Джерела

- [ChatGPT](https://chatgpt.com/?oai-dm=1)
- YouTube materials:
- [Web Servlets in Java](https://www.youtube.com/watch?v=0FpLve7ffoY&list=PLfu_Bpi_zcDOn8ajnuLY6g1C6hc_eeDFl&ab_channel=KodySimpson)
- [Servlet and JSP Tutorial for Beginner](https://www.youtube.com/watch?v=7TOmdDJc14s&list=PLsyeobzWxl7pUPF2xjjJiG4BKC9x_GY46&ab_channel=Telusko)
- [Servlet JSP Tutorial](https://www.youtube.com/watch?v=DzYyzmP4m5c&list=PLGRDMO4rOGcOjhFoLV2xOfRQqYjpSVhxt)
- [Уроки PostgreSQL для новачків](https://www.youtube.com/watch?v=PfyC39EzTmk&list=PLPPIc-4tm3YQsdhSV1qzAgDKTuMUNnPmp&ab_channel=%D0%90%D0%B2%D0%B5%D0%9A%D0%BE%D0%B4%D0%B5%D1%80)

Підказки ChatGpt були орієнтовно написання самого коду, нажаль скріншотів із будь яким змістом надати не можу, але я залишив достатньо інформації щодо того, як створити такого ж роду продукт.Дякую за Увагу!
